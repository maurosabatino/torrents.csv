#!/bin/bash

# Checking arguments
# Help line
torrents_csv="`pwd`/torrents.csv"

help="Run ./add_torrents.sh [TORRENTS_DIR] \nor goto https://gitlab.com/dessalines/torrents.csv for more help"
if [ "$1" == "-h" ] || [ -z "$1" ]; then
  echo -e $help
  exit 1
fi

torrents_dir="$1"
echo "Torrents dir=$torrents_dir"

# Check dependencies

if command -v transmission-cli >/dev/null 2>&1 ; then
  echo "transmission-cli version: $(transmission-cli --version)"
else
  echo "Install transmission-cli"
  exit 0
fi

if command -v "python -m torrent_tracker_scraper.scraper" >/dev/null 2>&1 ; then
  echo "torrent-tracker-scraper installed."
else
  echo -e "Installing torrent_tracker_scraper:\npip install torrent-tracker-scraper --user \nhttps://github.com/ZigmundVonZaun/torrent-tracker-scraper"
  pip install torrent-tracker-scraper --user
fi

if command -v "~/.local/bin/humanfriendly -c" >/dev/null 2>&1 ; then
  echo "humanfriendly installed."
else
  echo -e "Installing humanfriendly:\npip install humanfriendly --user"
  pip install humanfriendly --user
fi

# Loop over all torrents
pushd $torrents_dir
for torrent_file in *.torrent; do

  # Get fields from transmission
  
  show_text=$(transmission-show "$torrent_file")
  # echo "show text = $show_text"

  name=$(grep -Po -m 1 'Name: \K.*' <<< $show_text)
  name=$(sed 's/;/\\;/g' <<< $name) # Escape the commas for .csv

  # Size: Unfortunately this will chop off some sigfigs
  size=$(grep -Po 'Total Size: \K.*' <<< $show_text) 
  size_bytes=$(~/.local/bin/humanfriendly --parse-size="$size") # Convert to bytes

  infohash=$(grep -Po 'Hash: \K.*' <<< $show_text)

  # Convert the created date
  date_string=$(grep -Po 'Created on: \K.*' <<< $show_text)
  created_date=""
  if [[ "$date_string" == "Unknown" ]]; then
    created_date=$(date +%s)
  else
    created_date=$(date -d "${date_string}" +"%s")
  fi
  
  # Scrape for seeder counts
  scrape_text=$(timeout 20 python -m torrent_tracker_scraper.scraper \
    -i "$infohash" \
    -t tracker.coppersurfer.tk -p 6969)
    # -t tracker.internetwarriors.net -p 1337
    # -t tracker.opentrackr.org -p 1337
  seeders=$(grep -Po 'Seeds: \K[0-9]+' <<< $scrape_text)
  leechers=$(grep -Po 'Leechers: \K[0-9]+' <<< $scrape_text)
  completed=$(grep -Po 'Completed: \K[0-9]+' <<< $scrape_text)
  scraped_date=$(date +%s)

  # Construct add line
  add_line="$infohash;$name;$size_bytes;$created_date;$seeders;$leechers;$completed;$scraped_date"

  # Only add the line if there are seeds, and the infohash doesn't already exist
  if (( $seeders > 0 )); then

    # If the infohash already exists, replace the line
    found_line=$(rg -n $infohash $torrents_csv | cut -d : -f 1) 
    if [ ! -z $found_line ]; then
      sed -i "$found_line c$add_line" $torrents_csv
      echo -e "Found $name, updating peers"
    else
      # Append the add lines to the torrents.csv file
      echo -e "\n$add_line" >> $torrents_csv
      truncate -s -1 $torrents_csv # Removing last newline
      echo -e "Added $name"
    fi
  else 
    echo -e "$name has no seeders."
  fi

done

popd
. prune.sh

